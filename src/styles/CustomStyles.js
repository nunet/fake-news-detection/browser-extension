import {makeStyles} from '@material-ui/core';
import {createMuiTheme} from '@material-ui/core/styles';

export const theme = createMuiTheme({
    palette:{
      secondary:{
        main: '#00A79D'
      }
    }
})


export const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      flexDirection: 'column',
      width: '400px',
      height: '500px',
      borderRadius: '20px 20px 20px 20px',
    },
    content : {
      flex: 1,
      backgroundColor: 'white'
      //backgroundColor: '#0A2043',
    },
    appbar: {
      backgroundColor: 'white',
    },
    toolbar: {
      minHeight: '50px',
      backgroundColor: '#fafafa'
      //backgroundColor: '#233655',
    },
    footer : {
      backgroundColor: '#fafafa'
      //backgroundColor: '#233655',
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
      borderRadius: '18px 18px 18px 18px',
    },
    paperMax: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
      backgroundColor: '#00A79D',
      borderRadius: '18px 18px 18px 18px',
    },
  
    error: {
    },
    menuPaneText : {
      color: theme.palette.secondary,
    },
    panes : {
      backgroundColor: "#48A89D",
    },
    paneLow: {
      backgroundColor: '#233655',
      color: '#b3e5fc',
    },
    paneMedium: {
      //#fbc02d
      backgroundColor: '#48A89D',
    },
    paneHigh: {
      //#ff3d00 
      backgroundColor: '#309BD6',
    },
    
    frequencyCard : {
      borderRadius: '30px 30px 30px 30px',
    },
    expandWordFrequency: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandFrequencyOpen: {
      transform: 'rotate(180deg)',
    },
    
    info: {
      color: '#616161',
      '&:hover': {
          color: '#26a69a',
      }
    }
}));
  