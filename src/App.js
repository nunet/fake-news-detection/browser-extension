/* global chrome */

import React from 'react';
import {MuiThemeProvider} from '@material-ui/core/styles';

import MenuBar from './components/MenuBar';
import Footer from './components/Footer';
import MenuPane from './components/MenuPane';
import LoadingPane from './components/LoadingPane';
import DisplayUrl from './components/DisplayUrl';
import StatusPane from './components/StatusPane';
import DetailsPane from './components/DetailsPane';
import Error from './components/Error';
import RatingPane from './components/RatingPane';
import Analysis from './components/Analysis';
import History from './components/History';
import History2 from './components/History2';
import PrivacyNotice from './components/PrivacyNotice';

import {BrowserRouter as Router} from 'react-router-dom';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';

// import the custom styles
import {
  theme, 
  useStyles
} from './styles/CustomStyles';

import "./styles/style.scss";

// import the functions from the functionality with local database
import {
  LocalDB,
  getUrl, 
  getLoading, 
  getTimeTaken, 
  getErrorMsg, 
  getError, 
  getProbability,
  getCallId,
  getTraceId,
  getCICommitSHA,
} from './localdb/LocalDB';


function App(props){

    // call the use effect
    LocalDB();

    // the styles
    const classes = useStyles();

    // get the  values
    var currentUrl = getUrl();
    var loading = getLoading();
    var probability = getProbability();
    var timeTaken = getTimeTaken();
    var error = getError();
    var errorMsg = getErrorMsg();
    var callId = getCallId();
    var traceId = getTraceId();
    var ciCommitSHA = getCICommitSHA();

    return (
      <MuiThemeProvider theme={theme}>
        <Router>
          <div className={classes.root}>      
            {/* add menu bar */}
            <MenuBar classes={classes}/>

            {/* add a content */}
            <div className={classes.content}>
              
              {/* display url */}
              <Box pt={0.5} pl={2} pr={2}>
                  <DisplayUrl classes={classes} currentUrl={currentUrl} />
              </Box>

              {/* add container */}
              <Container fixed >
                {loading ? (
                  <Box pb={0.5}>
                    {error ? (
                      <Box>
                        {/* attach error pane here */}
                        <Box pt={2} pl={0} pr={2}>
                          <Error error={error} errorMsg={errorMsg} callId={callId} traceId={traceId} classes={classes} />
                        </Box>

                        {/* add history component */}
                        <Box pt={1} pr={1}>
                          <History2 />
                        </Box>  
                      </Box>
                    ): (
                    <Box>                      
                      {/* status pane that shows the probability */}
                      <Box pt={0.5} pl={4} pr={3}>
                        <StatusPane classes={classes} probability={probability} />
                      </Box>

                      {/* add details pane */}
                      <Box pt={0.5} pl={2} pr={0} >
                        <DetailsPane classes={classes} timeTaken={timeTaken} probability={probability} />
                      </Box>

                      {/* add rating component */}
                      <Box pt={0.5} pl={4}>
                          <RatingPane classes={props.classes} probability={props.probability} />
                      </Box>

                      {/* add view full analysis component */}
                      <Box pt={0.5}>
                          <Analysis />
                      </Box>

                      {/* add history component */}
                      <Box pt={1} pr={1}>
                          <History />
                      </Box>
                    </Box>
                    )}
                  </Box>

                  /* if its still fetching the result */
                  ) : (
                  /* add loading pane */
                  <Box pb={4}>
                      <LoadingPane />
                  </Box>
                  )
                }    
              </Container>            
            
              {/* add footer */}
              <Box pt={1} pl={2} pr={4} pb={1}>
                  <PrivacyNotice />
              </Box>
            </div>
             
            <Footer classes={classes} ciCommitSHA={ciCommitSHA} />
          </div>
        </Router>        
      </MuiThemeProvider>
    );
}

export default App;
