import React, {useState} from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

import packageJson from '../../package.json';

// get the version from package.json
// this version in package.json should be the same with the version in manifest.json
const version = packageJson.version;

function Footer(props) {

	return(
		<Box align="center" className={props.classes.footer}>
			
			<Grid container spacing={1}>
				<Grid item xs={4}>
					<Box pt={0.5}><Typography variant="small" >CREATED BY</Typography></Box>	
				</Grid>

				<Grid item xs={4}>
					<Box pt={0.5}><Typography variant="small" >POWERED BY</Typography></Box>
				</Grid>
				
				<Grid item xs={4} align="center">
					<Box pt={0.5}><Typography variant="small" >VERSION</Typography></Box>
				</Grid>
			</Grid>

			<Grid container spacing={1}>
				<Grid item xs={4}>
					<Tooltip title="https://nunet.io/">
					<Link underline="none" href="https://nunet.io/" target="_blank">
						<Box align="center"><img src='./assets/img/nunet_logo.png' alt="" width="100px" height="30px" /></Box>
					</Link>
					</Tooltip>	
				</Grid>

				<Grid item xs={4}>
					<Tooltip title="https://singularitynet.io/">
					<Link underline="none" href="https://singularitynet.io/" target="_blank">
						<Box align="center" ><img src='./assets/img/snet_logo.jpeg' alt="" width="100px" height="30px"  /></Box>
					</Link>
					</Tooltip>
				</Grid>
				
				<Grid item xs={4}>
					<Box pt={1} align="center">
					<Typography variant="small" >{version}(<Link underline="none" color="secondary" href={"https://gitlab.com/nunet/fake-news-detection/browser-extension/-/commit/"+props.ciCommitSHA} target="_blank">source</Link>)</Typography>
					</Box>
				</Grid>
				
			</Grid>
		</Box>
	)
}

export default Footer;