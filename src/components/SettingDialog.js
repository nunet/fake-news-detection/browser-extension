import React, {useState, useEffect} from 'react';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import IconButton from '@material-ui/core/IconButton';
import HistoryIcon from '@material-ui/icons/History';
import InfoIcon from '@material-ui/icons/Info';
import GavelIcon from '@material-ui/icons/Gavel';


function SettingDialog(props) {
    
    return(
        <Box>
            <Dialog
                open={props.openSetting}
                onClose = {props.closeSettingDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
            
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                  <Box>
                      
                    <Link href="newpage/history.html" underline="none" target="_blank">
                        <Box display="flex"> 
                            <Box>
                                <IconButton color="secondary"><HistoryIcon /></IconButton>
                            </Box>
                            <Box pt={1.2}>
                                <Typography variant="subtitle1" color="secondary">History</Typography>
                            </Box>
                        </Box>
                    </Link>

                    <Link href="newpage/legalterms.html" underline="none" target="_blank">
                        <Box display="flex"> 
                            <Box>
                                <IconButton color="secondary"><GavelIcon /></IconButton>
                            </Box>
                            <Box pt={1.2}>
                                <Typography variant="subtitle1" color="secondary">Legal Terms</Typography>
                            </Box>
                        </Box>
                    </Link>

                    <Link href="newpage/about.html" underline="none" target="_blank">
                        <Box display="flex"> 
                            <Box>
                                <IconButton color="secondary"><InfoIcon /></IconButton>
                            </Box>
                            <Box pt={1.2}>
                                <Typography variant="subtitle1" color="secondary">About</Typography>
                            </Box>
                        </Box>
                    </Link>

                  </Box>
                </DialogContentText>
                </DialogContent>


            </Dialog>
        </Box>
    );
}

export default SettingDialog;