import React, {useState} from 'react';
import AppBar from '@material-ui/core/AppBar'
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import Icon from '@material-ui/core/Icon';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import SettingsIcon from '@material-ui/icons/Settings';

import SettingDialog from './SettingDialog';


function MenuBar(props) {
  const [openSetting, setOpenSetting] = useState(false); // state to open and close the menu/setting dialog

	{/* to set the opensetting state to true */}
	const openSettingDialog = () => {
        setOpenSetting(true);
    }
    
	{/* to set the opensetting state to false */}
  const closeSettingDialog = () => {
      setOpenSetting(false);
  }
  
	return(
	    <Box>
        <AppBar position="static" className={props.classes.appbar}>
            <Toolbar className={props.classes.toolbar} >
              <Grid container spacing={1}>
                <Grid item xs={1} >
                  <Box pt={0} pt={1}><Icon><img alt="" src="./assets/img/lyingface.png" width="30px" height="30px" /></Icon></Box>
                </Grid>

                <Grid item xs={9}>
                  <Box pl={0.5} pt={1.5} color="black">
                    <Typography 
                      style={{fontSize: 18}}
                      display="block"
                      color="black"
                    >Fake News Warning
                    </Typography>
                  </Box>
                </Grid>
              
                <Grid item xs={1} align="right">
                  {/* add setting icon to pop up menu */}
                  <IconButton color="secondary" onClick={openSettingDialog}><SettingsIcon /></IconButton>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          
          {/* append the setting Dialog here, which will be poped up when the icon clicked */}
          <SettingDialog openSetting={openSetting} closeSettingDialog={closeSettingDialog} />      
      </Box>
    )
}

export default MenuBar;