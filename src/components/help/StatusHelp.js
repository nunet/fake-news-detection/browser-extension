import React from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

function StatusHelp(props) {
    
    return(
        <div>
        <Dialog
            open={props.statusHelp}
            onClose={props.closeStatusHelp}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description">
              
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    <Grid container>
                      <Grid item xs={6}>
                        <Box pt={3}>
                        <Typography textAlign="bottom" variant="h6">Status</Typography>
                        </Box>
                      </Grid>
                      <Grid item xs={6}>
                        <Box align="right">
                          <IconButton onClick={props.closeStatusHelp}><CloseIcon /></IconButton>
                        </Box>
                      </Grid>
                    </Grid>
                    <Box pl={3}>
                      <Typography variant="subtitle1">
                      </Typography>
                    </Box>
                    
                </DialogContentText>
            </DialogContent>

        </Dialog>

        </div>
    );
}

export default StatusHelp;