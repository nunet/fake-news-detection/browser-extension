import React from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

function DetailsHelp(props) {
    
    return(
        <div>
        <Dialog
            open={props.detailsHelp}
            onClose={props.closeDetailsHelp}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            >
            <DialogContent id="alert-dialog-description">
              <Grid container>
                  <Grid item xs={1}></Grid>
                  <Grid item xs={9}>
                    <Box pt={1}>
                      <Typography variant="h6">{props.title}</Typography>
                    </Box>
                  </Grid>                      
                  <Grid item xs={2}>
                    <Box align="right">
                      <IconButton onClick={props.closeDetailsHelp}><CloseIcon /></IconButton>
                    </Box>
                  </Grid>
              </Grid>

              <Box>
                <Typography variant="body2">
                  {props.text}
                </Typography>
              </Box>
            </DialogContent>
        </Dialog>

        </div>
    );
}

export default DetailsHelp;