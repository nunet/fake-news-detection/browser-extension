import React, {useState} from 'react'
import Alert from '@material-ui/lab/Alert';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import CloseIcon from '@material-ui/icons/Close';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';

function PrivacyNotice(props){
    const [open, setOpen] = useState(true);
  
    return (
        <Box>
            <Collapse in={open}>
                <Alert
                severity="info"
                icon={false}
                action={
                    <IconButton
                    aria-label="close"
                    color="inherit"
                    size="small"
                    onClick={() => {
                        setOpen(false);
                    }}
                    >
                    <CloseIcon fontSize="inherit" />
                    </IconButton>
                }
                >
                <small>
                    This app anonymizes your data securely, No cookies are used.
                    Please read our  
                </small>
                &nbsp;
                <small>
                    <Link href="newpage/legalterms.html" color="secondary" underline="none" target="_blank">
                        Privacy Policy
                    </Link> for details.
                </small>
                </Alert>
            </Collapse>    
        </Box>
    );
}

export default PrivacyNotice;