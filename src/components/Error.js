import React, {useState} from 'react';
import Box from '@material-ui/core/Box';
import Alert from '@material-ui/lab/Alert';
import AlertTitle from '@material-ui/lab/AlertTitle';
import ReplayIcon from '@material-ui/icons/Replay';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';

import LoadingPane from './LoadingPane';

import {
  change_status
} from '../localdb/LocalDB';

/*
* 
*/
function Error(props) {
  
  // set error state to show error page or loading pane after clicking retry button
  const [error, setError] = useState(props.error);

  // get the jeager link with the current trace id
  const jaegerLink = "http://testserver.nunet.io:16686/trace/"+props.traceId;
  
  const retryClicked = () => {
    // call the change status function, which will change the status to pending, so that the url can be re fetched by the api
    change_status();
    
    // show the loading pane for 3 second and then close the extension window,
    // then when the user clicks on the extension icon the result page will be displayed
    setError(false);
    setTimeout(
      function(){
        window.close();
      }, 3000
    );
  }
  
    return(
        <div>
          {error ? (
          <Box>
            <Alert severity="error">
              <AlertTitle>Some Error occured</AlertTitle>
              {props.errorMsg}
              <Box>
                <Link href={jaegerLink} target="_blank" rel="noopener" color="secondary">Go to jaeger trace</Link>
                &nbsp;&nbsp;&nbsp;
                <Link href="newpage/logdisplay.html" target="_blank" rel="noopener" color="secondary">Get the full Log</Link>
              </Box>
            </Alert>
            
            <Box pt={2}>
                <Button
                    variant="contained"
                    color="secondary"
                    style={{width:350, fontSize:12, color:'white'}}
                    onClick={retryClicked}
                    endIcon={<ReplayIcon />}
                >Retry
                </Button>
            </Box>
          </Box>
          ) : (
            <Box pb={4}>
                <LoadingPane />
            </Box>
          )}
        </div>
      )
}

export default Error;