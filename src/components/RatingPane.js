/* global */

import React from 'react';
import { makeStyles } from '@material-ui/core';

import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Slider from '@material-ui/core/Slider';
import Grid from '@material-ui/core/Grid';

import {
    getUrl, 
    getRank,
    set_rank,
} from '../localdb/LocalDB';


const useStyles = makeStyles((theme) => ({
    track: {
        width: 300,
    }
}));

const marks = [
    {
        value: 0,
        label: 'Not Fake'
    },
    {
        value: 100,
        label: 'Fake'
    }
];

function valuetext(value) {
    return  `${value}C`;
}


function RatingPane(props) {
    
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    // 1st get the rank value from local db
    var rank = getRank();
    
    const rankingChange = (event, newValue) => {
        var rateText = document.getElementById("rateText");
        rateText.innerHTML = newValue + "%";
        setValue(newValue);
        // set value to local db
        set_rank(newValue);
    }

    return(
        <Box>
            <Typography align="center" id="rating-slider" variant="caption" gutterBottom>
                How would you rank the article content ?
            </Typography>

            <Box >
                <Typography variant="body2" color="secondary" id="rateText" align="center">{rank} %</Typography>
            </Box>
                
            <Box className={classes.track}>
                <Slider 
                    track={false}
                    aria-labelledby="rateText"
                    getAriaValueText={valuetext}
                    defaultValue={rank}
                    step={2}
                    marks={marks}
                    color="secondary"
                    onChange={rankingChange}
                />
            </Box>        
        </Box>
    );
}

export default RatingPane;