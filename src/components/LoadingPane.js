import React, { } from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';

function LoadingPane(props) {
	return(
	    <div className="loading-pane">
        <Grid container justify="center">
            <Box>
                <Box pt={3} align="center" color="black">
                    <Typography variant="h6">Processing ...</Typography>
                </Box>
                <Box pt={1} align="center"><img src="./assets/img/NuNet-Spinner.gif" alt="loading..." width="150px" height="150px" />
                </Box>
            </Box>
        </Grid>
        
        </div>
    );
}

export default LoadingPane;