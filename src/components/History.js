import React from 'react';

import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';

import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

import OpenInNew from '@material-ui/icons/OpenInNew';
import HistoryIcon from '@material-ui/icons/History';
import Report from '@material-ui/icons/Report';

import {
    getUrl,
  } from '../localdb/LocalDB';

function History(props) {

    const historyText = "View full history of browser extension calls to singularityNet algorihms and access analysis and  details of each callview full history of browser extension calls to singularityNet algorihms and access analysis and  details of each call";
    const reportText = "Provide feedback about the precision of detection or Report issue on the page";

    // get the url
    var currenturl = getUrl();

    // set the details href of the view analysis link
    const detailsHref = "newpage/details.html?url="+currenturl;
    
    return(
        <Box>
            <Box>
                <Tooltip title={historyText}>
                    <Box>
                        <Link href="newpage/history.html" target="_blank" underline="none">
                        <Button
                            variant='outlined'
                            color='secondary'
                            style={{width:350, fontSize:12}}
                            endIcon={<HistoryIcon />}
                        > VIEW FULL CALL History
                        </Button>
                        </Link>
                    </Box>
                </Tooltip>
                {/*
                <Tooltip title={reportText}>
                    <Box pl={1.5}>
                        <Link href={detailsHref} target="_blank" underline="none">
                        <Button
                            variant='outlined'
                            color='secondary'
                            style={{fontSize:11}}
                            endIcon={<Report />}
                        > Report issue on this page
                        </Button>
                        </Link>
                    </Box>
                </Tooltip>
                */}
            </Box>
        </Box>
    );
}

export default History;