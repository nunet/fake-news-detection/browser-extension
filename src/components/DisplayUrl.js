import React, {useState} from 'react';

import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Card from '@material-ui/core/Card';

function DisplayUrl(props) {

    const showurl = props.currentUrl.substring(0,100);

    return(
        <Card elevation={0}>
            <Grid container spacing={1}>
                <Grid item xs={3} align="center">
                    <Typography variant="subtitle2" align="center">This website: </Typography>
                </Grid>
                <Grid item xs={9} align="left">
                    <Box>
                        <Typography variant="caption" align="left">{showurl}</Typography>
                    </Box>
                </Grid>
            </Grid>
        </Card>
    );
}
export default DisplayUrl;