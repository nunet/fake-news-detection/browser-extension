import React, {useState} from 'react';

import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import InfoIcon from '@material-ui/icons/Info';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import Link from '@material-ui/core/Link';


import DetailsHelp from './help/DetailsHelp';

import {
    getAlgList
} from "../localdb/LocalDB";


function DetailsPane(props) {

    // get the algorithims list
    var algList = getAlgList();
    
    // set the state
    const [detailsHelp, setDetailsHelp] = useState(false);
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");

    // check window onclick
    window.onclick = e => {
        // get the id from the clicked event
        console.log(e)
        var id = "";
        try {
            if(e.target.id) {
                id = e.target.id
            } else if (e.target.farthestViewportElement.id) {
                id = e.target.farthestViewportElement.id
            }
        } catch (error) {
            //console.log("error: ", error)
        }
        // check the id of the event with the value in the data
        for (var i=0; i < algList.length; i++){
            if(id == algList[i].human_readable || id == algList[i].human_readable+"-1" || id == algList[i].human_readable+"-2"){
                setDescription(algList[i].description)
                setTitle(algList[i].human_readable)
                setDetailsHelp(true);
            }
        }
    }

    {/* function to close details help dialog */}
    const closeDetailsHelp = () => {
        setDetailsHelp(false);
    }   

    {/* function to insert dash(character) in the string at nth index  
    * this used to display algorithim result on the extension page, so that it can goes down than 
    */}
    const displayAlgorithims = (algList) => {
        // initialize the variable to hold the list of UI algorithims
        var final = [];
        for (var i=0; i < algList.length; i++){
            var nam = algList[i].name
            var humanReadable = algList[i].human_readable
            var result = algList[i].return
            
            // if the algorithim name is testing-news-score, skip over it because its already displayed
            if (nam == "news-score" || nam == "testing-news-score"){
                // skip
            }
            else {
                final.push(
                    <Box key={nam} display="flex" pb={1}>
                        {/* add info icon and text */}
                        <Link href="#" underline="none">
                            <Box width={200} pt={1} className={props.classes.info}  display="flex">
                                <Box pr={1} id={humanReadable}>
                                    <InfoIcon id={humanReadable+"-1"}  />
                                </Box>   
                                <Box><Typography variant="body1" id={humanReadable+"-2"}>{humanReadable}</Typography></Box>
                            </Box>
                        </Link>
        
                        {/* stance result */}
                        <Box width={150} pl={1} pt={0.5}>
                            <Grid item xs={11}>
                                <Card elevation={0} style={{backgroundColor:'#eeeeee'}}>                            
                                    <Box p={0.5}>
                                        <Typography variant="body1">{result}</Typography>
                                    </Box>
                                </Card>            
                            </Grid>
                        </Box>    
                    </Box>
                );
            }
        }

        return(
            <Box>{final}</Box>
        );
    } // end of display algorithims

    return(
        <Box>
            {/* 
             add displaying algorithims function 
             */}
            {displayAlgorithims(algList)}

            {/* attach details help 
            * 
            * This help text will be retrieved from the backend with the algorithim
            */}
            <DetailsHelp title={title} text={description} detailsHelp={detailsHelp} closeDetailsHelp={closeDetailsHelp} />
        </Box>
    );
}

export default DetailsPane;