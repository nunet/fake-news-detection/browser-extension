import React from 'react';

import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';

import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

import OpenInNew from '@material-ui/icons/OpenInNew';

import {
    getUrl,
  } from '../localdb/LocalDB';

function Analysis(props) {

    const analysisText = "View full details and analysis of the inspection of this page by SingularityNet algorithms";
    
    // get the url
    var currenturl = getUrl();

    // set the details href of the view analysis link
    const detailsHref = "newpage/details.html?url="+currenturl;
    
    return(
        <Box>
            <Box>
                <Tooltip title={analysisText}>
                    <Link href={detailsHref} target="_blank" underline="none">
                        <Button
                            variant="contained"
                            color="secondary"
                            style={{width:350, fontSize:12, color:'white'}}
                            endIcon={<OpenInNew />}
                        >VIEW FULL ANALYSIS OR REPORT ISSUE
                        </Button>
                    </Link>
                </Tooltip>
            </Box>    

        </Box>
    );
}

export default Analysis;