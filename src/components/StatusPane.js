import React from 'react';
import $ from 'jquery';

import Box from '@material-ui/core/Box';

function StatusPane(props) {
    {/* get the text */}
    var probability = props.probability;
    var text = 
        probability >= 70 ? 
        "High" :
        probability >= 40 ?
        "Medium" :
        "Low";
    
    var pathColor = 
        text == "High" ?
        "#E67381" :
        text == "Medium" ?
        "#F7C26B" :
        "#59BB6E";

    // jquery function to fill the respective color until the probability number within 3 seconds
    $(function(){    
        $(".progress").each(function(){
            var $bar = $(this).find(".bar");
            var perc = parseInt(probability, 10);
            $({p:0}).animate({p:perc}, {
            duration: 3000,
            easing: "swing",
            step: function(p) {
                $bar.css({
                    transform: "rotate("+ (45+(p*1.8)) +"deg)", // 100%=180° so: ° = % * 1.8
                    // 45 is to add the needed rotation to have the colored borders at the bottom
                    borderBottomColor: pathColor,  /* half azure */
                    borderRightColor: pathColor,
                });
            }
            });
        }); 
    });

    return(
        <div>    
            <Box>
                <div class="progress" align="center">
                <div class="barOverflow">
                    <div class="bar"></div>
                </div>
                <div class="progressText">
                    <h2>{text}</h2>
                    <h3>Fake Probability</h3>
                    <p class="sText">{probability}%</p>
                </div> 
                </div>
            </Box>
        </div>
    );
}

export default StatusPane;