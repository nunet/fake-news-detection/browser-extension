import React from 'react';

import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';

import FeedbackIcon from '@material-ui/icons/Feedback';
import HistoryIcon from '@material-ui/icons/History';
import InfoIcon from '@material-ui/icons/Info';
import GavelIcon from '@material-ui/icons/Gavel';


function MenuPane(props) {

  return(
    <Box pt={1} pl={5} display="flex" align="center">
        <Box>
        <Link href="newpage/feedback.html"  underline="none" target="_blank">
            <Box>
                <IconButton color="secondary"><FeedbackIcon /></IconButton>
                <Typography variant="subtitle1" color="secondary">Feedback</Typography>
            </Box>
        </Link>
        </Box>
        <Box pl={2}>
            <Link href="newpage/history.html"  underline="none" target="_blank">
                <Box>
                    <IconButton color="secondary"><HistoryIcon /></IconButton>
                </Box>
                <Box>
                    <Typography variant="subtitle1" color="secondary">History</Typography>
                </Box>
            </Link> 
        </Box>

        <Box pl={2}>
        <Link href="newpage/legalterms.html" underline="none" target="_blank">
                <Box>
                    <IconButton color="secondary"><GavelIcon /></IconButton>
                </Box>
                <Box>
                    <Typography variant="subtitle1" color="secondary">Legal Terms</Typography>
                </Box>
        </Link>
        </Box>

        <Box pl={2}>
        <Link href="newpage/about.html" underline="none" target="_blank">
                <Box>
                    <IconButton color="secondary"><InfoIcon /></IconButton>
                </Box>
                <Box>
                    <Typography variant="subtitle1" color="secondary">About</Typography>
                </Box>
        </Link>
        </Box>

    </Box>
  )
}

export default MenuPane;