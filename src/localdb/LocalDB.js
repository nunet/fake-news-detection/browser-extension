/* global chrome */

import { useState, useEffect } from 'react';

import {v4 as uuidv4} from "uuid"; // generate universal unique identifier for each call
import {DeviceUUID}  from 'device-uuid'; // generate the device(computer UUID) which is independent of browser

// initialize the parameters to export them as function with get and set
var url1;
var loading1;
var algList1;
var timeTaken1;
var errorMsg1;
var error1;
var probability1;
var onProgress1;
var rank1;
var callUUID = uuidv4(); // which generated here in browser
var deviceUUID = new DeviceUUID().get();
var call_id1; // which comes from the backend
var trace_id1;
var ciCommitSHA1;

console.log("device uuid", deviceUUID);
console.log("call uuid", callUUID);

export function LocalDB() {

    // state used here to update the values
    const [currentUrl, setCurrentUrl] = useState('');
    const [loading, setLoading] = useState(false);
    const [algList, setalgList] = useState();
    const [rank, setRank] = useState();
    const [timeTaken, setTimeTaken] = useState();
    const [errorMsg, setErrorMsg] = useState('');
    const [error, setError] = useState(false);
    const [probability, setProbability] = useState();
    const [onProgress, setOnProgress] = useState(false);
    const [callId, setCallId] = useState();
    const [traceId, setTraceId] = useState();
    const [ciCommitSHA, setCICommitSHA] = useState();
    
    useEffect(() => {    
        // getting the url of the tab and call the function to check weather this url is found in the local data or not
        // some url of the tab is counted as Invalid Url e.g like brave://extensions/ in brave browser and empty new tab
        chrome.tabs.query(
          {active: true, currentWindow: true},
          tabs => {
            try {
                var link = new URL(tabs[0].url);
                link = link.toString();
                // check if the link(article) has ?(get query parameters) parameters or #(anchor) and get only the main address 
                // since error occured with the ?(get query parameter) or #(anchor) going to be added while requesting api
                if(link.includes("?")) {
                    link = link.split("?")[0];
                }
                if(link.includes("#")) {
                  link = link.split("#")[0];
                }

                console.log("the link is", link);
                setCurrentUrl(link);
                check_local(link);
                //setLoading(true);
            } catch(error) {
                setCurrentUrl(link);
                setLoading(true);
                setError(true)
                setErrorMsg(error.toString());
              }
        });
        
    },[]);

    /*
    * function to check for local background
    */
    const check_local =  (currenturl) => {
      // check from chrome extension local
      
      // 1st check if url already found in the data, if so display, if not set the data and let fetch.js fetch the api and set the value
      chrome.storage.local.get(['NData'], function(result) {
        const parseData = result['NData'];
        var urlfound = false; // variable to check weather the current url is found or not in the local data
        var probability; // variable to hold probability
        var algResult; // variable to hold the algorithims result
        var rank; // variable to hold the rank value
        var time_taken; // variable to hold the time taken
        var pending = false;
        var error_message; // variable to hold error
        var call_id; // variable to hold call id
        var trace_id; // variable to hold trace id
          
        
        for (const elt in parseData) {
            var storedurl = parseData[elt].url;
            var status = parseData[elt].status;
        
            if(currenturl == storedurl) {
                urlfound = true;
                
                if (status == "pending") {
                    pending = true; 
                } 

                else {
                    probability = parseData[elt].probability;
                    rank = parseData[elt].rank; 
                    time_taken = parseData[elt].time_taken;
                    error_message = parseData[elt].error;
                    call_id = parseData[elt].call_id;
                    trace_id = parseData[elt].trace_id;
 
                    algResult = parseData[elt].algorithims;
                    console.log("algorithim result 2: ", algResult)

                    for (var i=0; i < algResult.length; i++){
                        var name = algResult[i].name;                        
                        var value; // variable to hold the value
                        // get the return dictionary of the algorithim
                        var alg_return = algResult[i].return;
                        // get the list of values and keys
                        var return_values = Object.values(alg_return);
                        var return_keys = Object.keys(alg_return);
      
                        if (name === "uclnlp" || name === "testing-uclnlp") {
                            // get the index of maximum values 
                            var indx = return_values.indexOf(Math.max(...return_values));  
                            // get the stance text correspond to the maximum values 
                            value = return_keys[indx];
                            // change the first letter to upper case
                            value = value[0].toUpperCase() + value.substring(1);
                        } else {
                            // get the value
                            value = return_values[0];
                        }
                        // change the return to single value so that to display on extension window
                        algResult[i].return = value
                        // delete the telemetery from algorithim result as we dont need to display in extension window
                        delete algResult[i].telemetry
                    } 
                }
            } 
        }

        if (urlfound && pending){
            // if urlfound and status is pending, call the get data directly
            // the get data will gonna loop again and again in local data until it gets the value with status = done
            console.log("call get data from pending status");
            get_data(currenturl);
        }
        else if (urlfound && !pending) {
            // if the url found and the status is done and there is no error, show the result
            if (error_message === ""){
                console.log("data retreived");
                setProbability(probability);
                setalgList(algResult);
                setRank(rank);
                setTimeTaken(time_taken);
                setCallId(call_id);
                setTraceId(trace_id);  
                setLoading(true);
            }
            else {  
              // if there is error, show the error message
              //console.log("error ocured: ", error_message);
              setErrorMsg(error_message);
              setCallId(call_id);
              setTraceId(trace_id);    
              setError(true);
              setLoading(true);
            }
        } 
        else {
            // first change chrome extension icon to the pending icon
            chrome.browserAction.setIcon({path: "./assets/img/loading-icong1.jpg"});
            // check weather that there is already another article on progress to store into the local db
            //check_pending_availability(currenturl);

            // set the url(article) into the local db
            set_data(currenturl);
        }

        // then get the ci gitlab job url
        chrome.storage.local.get(['GitlabCIInfo'], function(result) {
            var pData = result['GitlabCIInfo'];
            var ci_commit_sha = pData["ci_commit_sha"];
            setCICommitSHA(ci_commit_sha);
        });
      });
    }
   
    /*
    * check that weather the another article(tab) is on progress(calling)
    * it means if the result is already being processed
    */
    const check_pending_availability = (currenturl) => {
      console.log("checking fetching starts");
      
      // 1st check if url already found in the data, if so display, if not set the data and let fetch.js fetch the api and set the value
      chrome.storage.local.get(['NData'], function(result) {
        const parseData = result['NData'];
        var fetching = false;
        for (const elt in parseData) {
            var status = parseData[elt].status;
            console.log("status: ", status); 
            if (status == "pending") {
                fetching = true; 
            } 
        }
        if (fetching === true){
          console.log("waiting for the other article(tab) to finish");
          // dispaly that another article(tab) is on progress
          setOnProgress(true);

          // and loop through local database until the other article(tab) finished.
          setTimeout(
            function(){
              check_pending_availability(currenturl);
            }, 1000
        );
          
        }
        else {
          setOnProgress(false);
          // set the local storage with status pending
          set_data(currenturl);
        }
      });
    }
       
    /*
    *
    */
    const set_data = (currenturl) => {

        // then update the local with latest value
        const status="pending";
        const resultobject = {url:currenturl, 
                              callUUID:callUUID,
                              deviceUUID:deviceUUID,
                              status:status, 
                            };
        // add data
        add_data(currenturl, resultobject);
    }

    const add_data = (currenturl, resultobject) => {
        chrome.storage.local.get(['NData'], function(result) {
          const parseData = result['NData'];
          // create list to hold list of data
          var listdata = [];
          // if data is not empty loop through all and add to the list
          if (parseData){
            for (const elt in parseData) {
              listdata.push(parseData[elt]);
            }
          }
          // push the current one the list
          listdata.push(resultobject);

          chrome.storage.local.set({'NData':listdata}, function() {
              if(chrome.runtime.lastError){
                console.log("Error from extension window while pushing to local database: ", chrome.runtime.lastError);
                return;
              }
              console.log("data is stored", listdata);
              // then call the get data from initial status, 
              // the get data will gonna loop again and again in local data until it gets the value with status = done
              console.log("call get data from pending status");
              get_data(currenturl);
          }); 
        });
    }

    const get_data = (currenturl) => {
        // 1st check if url already found in the data, if so display, if not set the data and let fetch.js fetch the api and set the value
        chrome.storage.local.get(['NData'], function(result) {
          const parseData = result['NData'];

          var urlfound = false; // variable to check weather the current url is found or not in the local data
          var probability; // variable to hold the general probability
          var algResult; // variable to hold the algorithims result
          var rank; // variable to hold the rank value
          var time_taken; // variable to hold the time taken
          var error_message; // variable to hold error
          var call_id; // variable to hold call id
          var trace_id; // variable to hold trace id
          
          for (const elt in parseData) {
            var storedurl = parseData[elt].url;
            var status = parseData[elt].status;
            if((currenturl == storedurl) && (status == "done")) {
              urlfound = true;
              probability = parseData[elt].probability;
              rank = parseData[elt].rank; 
              time_taken = parseData[elt].time_taken;
              error_message = parseData[elt].error;
              call_id = parseData[elt].call_id;
              trace_id = parseData[elt].trace_id;
 
              algResult = parseData[elt].algorithims;
              console.log("algorithim result 1: ", algResult)
              
              for (var i=0; i < algResult.length; i++){
                var name = algResult[i].name;                
                var value; // variable to hold the result
                // get the return dictionary of the algorithim
                var alg_return = algResult[i].return;
                // get the list of values and keys
                var return_values = Object.values(alg_return);
                var return_keys = Object.keys(alg_return);

                if (name === "uclnlp" || name === "testing-uclnlp") {
                    // get the index of maximum values 
                    var indx = return_values.indexOf(Math.max(...return_values));  
                    // get the stance text correspond to the maximum values 
                    value = return_keys[indx];
                    // change the first letter to upper case
                    value = value[0].toUpperCase() + value.substring(1);
                } else {
                    // get the value
                    value = return_values[0];
                }
                // change the return to single value so that to display on extension window
                algResult[i].return = value
                // delete the telemetery from algorithim result as we dont need to display in extension window
                delete algResult[i].telemetry
             }    
            }
          }

          // if urlfound, show the result
          // if not, loop through the local data
          if (urlfound){
              // if there is no error
              if (error_message === ""){
                  setProbability(probability);
                  setalgList(algResult);
                  setRank(rank);
                  setTimeTaken(time_taken);
                  setCallId(call_id);
                  setTraceId(trace_id);
                  setLoading(true);
              } // if error occured
              else {
                  setErrorMsg(error_message);
                  setCallId(call_id);
                  setTraceId(trace_id);
                  setError(true);
                  setLoading(true);
              }
          } else {
              // loop through check local again and again to look that the background script update the data in every second
              // while looping it shows only the loading spinner
              console.log("checking get data 2");
              setTimeout(
                function(){
                  get_data(currenturl);
                }, 2000
            );
          }
        });
    }
    
    // update the values from the values in the state
    url1 = currentUrl;
    loading1 = loading;
    algList1 = algList;
    timeTaken1 = timeTaken;
    errorMsg1 = errorMsg;
    error1 = error;
    probability1 = probability;
    rank1 = rank;
    onProgress1 = onProgress;
    call_id1 = callId;
    trace_id1 = traceId;
    ciCommitSHA1 = ciCommitSHA;

}

export function getUrl(){  
  return url1;
}

export function getLoading(){
  return loading1;
}


export function getTimeTaken(){
  return timeTaken1;
}

export function getErrorMsg(){
  return errorMsg1; 
}

export function getError(){
  return error1;
}

export function getProbability(){
  return probability1;
}

export function getOnProgress(){
  return onProgress1;
}

export function getRank(){
    return rank1;
}

export function getCallId(){
  return call_id1;
}

export function getTraceId(){
  return trace_id1;
}

export function getCallUUID(){
  return callUUID;
}

export function getDeviceUUID(){
  return deviceUUID;
}

/*
* get the updated rank value and set it to the local database
*/
export function set_rank(value) {
    // get the data and update it
    chrome.storage.local.get('NData', function(result){
      var parseData = result['NData'];
      for (var x in parseData) {
        var db_url = parseData[x].url;
        if (url1 === db_url){
            parseData[x].rank = value;
        }
      }
        
      // set the updated data to local
      chrome.storage.local.set({'NData':parseData}, function() {
        if(chrome.runtime.lastError){
          console.log("Error from extension window while storing rank: ", chrome.runtime.lastError);
          return;
        }
          console.log("rate is updated");
          console.log("data: ", parseData);
      });
    });
}

/*
* get algorithim list
*/
export function getAlgList(){
    return algList1;
}

export function change_status(){
    // get the data and update it
    chrome.storage.local.get('NData', function(result){
      var parseData = result['NData'];
      for (var x in parseData) {
        var db_url = parseData[x].url;
        if (url1 === db_url){
            parseData[x].status = "pending";
        }
      }
        
      // set the updated data to local
      chrome.storage.local.set({'NData':parseData}, function() {
        if(chrome.runtime.lastError){
          console.log("Error from extension window while changing status to pending: ", chrome.runtime.lastError);
          return;
        }
          console.log("status changed");
          console.log("data: ", parseData);
      });
    });
}

export function getCICommitSHA(){
  return ciCommitSHA1;
}