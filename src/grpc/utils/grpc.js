// import grpc web
const grpc = require("grpc-web");

//import function from proto file
const { NunetAdapterClient } = require("../proto/nunet_adapter_grpc_web_pb");
const { ServiceDefnition} = require("../proto/nunet_adapter_pb");

 /*
  * Function to call grpc method
  */
 const call_grpc = async (url) => {
    
    // initialize the proto variable
    const service_name  = "fn_score_calc";
    const params = url; 

    //create reusable grpc client with grpc host
    //let client = new NunetAdapterClient(process.env.REACT_APP_GRPC_HOST);
    const enableDevTools = window.__GRPCWEB_DEVTOOLS__ || (() => {});
    const client = new NunetAdapterClient(process.env.REACT_APP_GRPC_HOST);
    enableDevTools([
      client,
    ]);
    
    // make the request to the grpc
    const request = new ServiceDefnition();

    // set the input to the grpc
    request.setServiceName(service_name);
    request.setParams(params);

    // get the response from grpc server
    client.reqService(request, {}, (error, response) => {
      if(error) {
          setError(true);
          setErrorMsg('Error Occured');
          console.log("error get values", error);
          setLoading(true);
      }
      else {
          const result = response.getServiceResponse();
          console.log("result from server", result);  
          const value_list = result.split('\n');
          const agree = value_list[2].split(': ')[1].replace(',','');
          const disagree = value_list[3].split(': ')[1].replace(',','');
          const discuss = value_list[4].split(': ')[1].replace(',','');
          const unrelated = value_list[5].split(': ')[1].replace(',','');
          const values = [agree, disagree, discuss, unrelated];
          console.log(values);
          setScorevalue(values);
          setLoading(true);
      }
    });
    
  } 


export {
    call_grpc
};



