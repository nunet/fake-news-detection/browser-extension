/**
 * @fileoverview gRPC-Web generated client stub for 
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = require('./nunet_adapter_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.NunetAdapterClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.NunetAdapterPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.ServiceDefnition,
 *   !proto.ServiceResponse>}
 */
const methodDescriptor_NunetAdapter_reqService = new grpc.web.MethodDescriptor(
  '/NunetAdapter/reqService',
  grpc.web.MethodType.UNARY,
  proto.ServiceDefnition,
  proto.ServiceResponse,
  /**
   * @param {!proto.ServiceDefnition} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ServiceResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ServiceDefnition,
 *   !proto.ServiceResponse>}
 */
const methodInfo_NunetAdapter_reqService = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ServiceResponse,
  /**
   * @param {!proto.ServiceDefnition} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.ServiceResponse.deserializeBinary
);


/**
 * @param {!proto.ServiceDefnition} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ServiceResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ServiceResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.NunetAdapterClient.prototype.reqService =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/NunetAdapter/reqService',
      request,
      metadata || {},
      methodDescriptor_NunetAdapter_reqService,
      callback);
};


/**
 * @param {!proto.ServiceDefnition} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ServiceResponse>}
 *     Promise that resolves to the response
 */
proto.NunetAdapterPromiseClient.prototype.reqService =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/NunetAdapter/reqService',
      request,
      metadata || {},
      methodDescriptor_NunetAdapter_reqService);
};


module.exports = proto;

