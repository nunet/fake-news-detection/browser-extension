console.log("--------------background is running---------------");

// call the initial functions
back_get_data();
back_check_status();

get_gitlab_ci_info();

// total logs list to store all logs and then finally to store into the local log database
// with url and timestamp
var total_logs = [];

// Get the active tab url name to change the icon of the browser extension based on the status
// Check weather the url is found in the local database, 
// If found check status weather its pending or done, if found and pending show pending icon, if not show original icon
chrome.tabs.onActivated.addListener((activeInfo) => {
  chrome.tabs.query({'active': true, 'lastFocusedWindow': true, 'currentWindow': true}, function (tabs) {
      var currenturl = tabs[0].url;
      // check if the link(article) has ?(get query) parameters or anchor(#) and get only the main address 
      // since error occured with the ?(get query parameter) or anchor(#) going to be added while requesting api
      if(currenturl.includes("?")) {
        currenturl = currenturl.split("?")[0];
      }
      if(currenturl.includes("#")) {
        currenturl = currenturl.split("#")[0];
      }
      console.log("current active url: ", currenturl);
      chrome.storage.local.get(['NData'], function(result) {
        const parseData = result['NData'];
        var urlfound = false;
        var pending = false;
        for (const elt in parseData) {
            var storedurl = parseData[elt].url;
            var status = parseData[elt].status;       
            if(currenturl == storedurl) {
                urlfound = true;
                if (status == "pending") {
                    pending = true; 
                } 
            } 
        }
        if (urlfound && pending){
            // change chrome extension icon to the pending icon
            chrome.browserAction.setIcon({path: "./assets/img/loading-icong1.jpg"});
        }
        else { 
            // change icon to the original
            chrome.browserAction.setIcon({path: "./assets/img/lyingface.png"});
        }
      });

  });
});

/**
 * Function to get the gitlab ci information from file and store it in the database
 * 
 * @params 
 */
function get_gitlab_ci_info(){
    // read from file
    fetch('info.txt')
    .then(response => response.text())
    .then(text => {
        let info = text.split("\n");
        var ciCommitShaLine = info[1];
        console.log(ciCommitShaLine);
        var ciCommitSha= ciCommitShaLine.split("=>")[1];
        var result = {ci_commit_sha:ciCommitSha};
        chrome.storage.local.set({'GitlabCIInfo':result}, function() {
            if(chrome.runtime.lastError){
              console.log("Error from background js while pushing gitlab ci info to local database: ", chrome.runtime.lastError);
              return;
            }
            console.log("gitlab ci info is stored");
        });
    });
}

/*
* get the data from local database
*/
function back_get_data() {
  console.log("get data");
  chrome.storage.local.get('NData', function(result) {
      const parseData = result['NData'];
      console.log('data, ' + parseData);
  });
}

/*
* clean the local database
*/
function back_remove_data() {
  chrome.storage.local.remove(['NData'], function() {
      console.log('removed');
  });
}

/*
* check the calling status in the local database again and again
* to check weather the extension icon is clicked or not for each 2 seconds, 
* so that the background script can call the api
*/
function back_check_status() {
    // check from chrome extension local
    console.log("checking status----------");
   // 1st check if url already found in the data, if so display, if not set the data and let fetch.js fetch the api and set the stance detection value
   chrome.storage.local.get(['NData'], function(result) {
     const parseData = result['NData'];
     var pending = false; // variable to check weather the current url is found or not in the local data
     var currenturl = "";
     var callUUID;
     var deviceUUID;
     for (const elt in parseData) {
       var status = parseData[elt].status;
       if(status == "pending") {
          pending = true;
          currenturl = parseData[elt].url;
          callUUID = parseData[elt].callUUID;
          deviceUUID = parseData[elt].deviceUUID;
       }
     }
     // if the url is found and its status is pending then call the fetch api and
     if (pending){
         total_logs.push("Article to be fetched found: " + currenturl);
         console.log("pending url found---");
         back_fetch_api(currenturl, callUUID, deviceUUID);
         //json_test_result(currenturl, callUUID, deviceUUID);
     } else {
         // loop again and again to check the local data, the user may click the icon and data can be setted
         setTimeout(
            function(){
              back_check_status();
            }, 1000
         );
     }
   });
 }

/*
* set the json result manually instead of calling the api, for the purpose of testing
*/
function json_test_result(currenturl, callUUID, deviceUUID) {
    // get the start time
    var start_time = new Date().getTime();
    // set the test result
    var stringjson = `{
      'url': 'https://www.bbc.com/news', 
      'timestamp': 1621415608.6473393, 
      'result': {
        'general_probability': -0.06337174820000001, 
        'algorithms': [
          {'name': 'uclnlp', 'return': \"{'agree': 0.28646868, 'disagree': 0.25193137, 'discuss': 0.3719607, 'unrelated': 0.089639269}\", 'telemetry': \"{'memory used': 5.978862762451172, 'cpu used': 0.030429999999796564, 'network used': 7.75, 'time_taken': 0.002643108367919922}\"},
          {'name': 'binary-classification', 'return': \"{'binary_classification_result': 1}\", 'telemetry': \"{'memory used': 4.075046539306641, 'cpu used': 12.16099000000031, 'network used': 109.2421875, 'time_taken': 2.27372407913208}\"},
          {'name': 'news-score', 'return': \"{'general_probability': -0.06337174820000001}\"}
          ], 
        'escrow_info': [
          {'service': 'uclnlp', 'escrow_address': '0x96C56722302a69dc65514CefBDcb0826C926A858', 'cost': 75}, 
          {'service': 'binary-classification', 'escrow_address': '0xe0e66F32725cca93363F1dA022826C184d08Ae3E', 'cost': 125}, 
          {'service': 'news-score', 'escrow_address': '0xa96674b98378AE486Bcd78F18A7BDED86d1a227A', 'cost': 25}
        ]
      }}`;
    stringjson = stringjson.replaceAll(`"`, ``);
    stringjson = stringjson.replaceAll(`'`, `"`);
    console.log(stringjson);
    var obj = JSON.parse(stringjson);
    // extract the url
    var url_result = obj.url;
    console.log("url result : ", url_result);
    var timestamp = obj.timestamp;
    var probability = obj.result.general_probability;
    console.log("general probability: ", probability);
    var alg_result = obj.result.algorithms;
    console.log("algorithims: ", alg_result);
    var escrow_info = obj.result.escrow_info;
    console.log("escrow info: ", escrow_info);
    // get the final time
    var final_time = new Date().getTime();
    // get the total time it takes to fetch the result in seconds
    var time_taken = (final_time - start_time) / 1000;
    var error = "";
    // set data to local storage
    back_set_data(currenturl, callUUID, deviceUUID, probability, time_taken, alg_result, escrow_info, error);
}

/*
* Get the result from the api
*/
function back_fetch_api(currenturl, callUUID, deviceUUID) {
    // get data from api
    // first change chrome extension icon to the pending icon
    chrome.browserAction.setIcon({path: "./assets/img/loading-icong1.jpg"});
    const prod_api_endpoint = "http://demo.nunet.io:7006/";
    const test_api_endpoint = "http://135.181.222.170:7005/";
    const method = "get_score";
    const url_endpoint = prod_api_endpoint+method+"?url="+currenturl+"&callUUID="+callUUID+"&deviceUUID="+deviceUUID;
    console.log("--------making request---------");
    console.log(url_endpoint);
    total_logs.push("-----Making request----");
    total_logs.push(url_endpoint);
    // get the start time
    var start_time = new Date().getTime();
    //initialize the variable needed
    var errorMsg = ""; // initialize error message
    var alg_result = []; // set algorithim result class
    var escrow_info = []; // set escrow info
    var agi_info = []; // set agi info
    var probability = 0.00;
    var time_taken = 0.00;
    var call_id = "";
    var trace_id = "";
    var result;

    // fetch the rest api
    fetch(url_endpoint)
    .then(response => response.json())
    .then(json => {
        total_logs.push("----The Full raw result----");
        total_logs.push(json);
        console.log("----The Full raw result----");
        console.log(json);   
        // get the result value from the key call
        result = json.call;
        call_id = json.call_id;
        trace_id = json.trace_id;
        // remove the inappropriate double qoute which makes the dictionary value to be string, 
        // like "return":"{...}"     ,    "telemetry":"{...}"
        result = result.replaceAll(`"`, ``);
        // replace the single qoute with double qoute since single qoute has some issues while doing JSON parsing
        result = result.replaceAll(`'`, `"`);
        // parse the result into json format
        var obj = JSON.parse(result);            
        total_logs.push("----Result returned and successfully parsed---");
        console.log("----Result successfully returned---");
        // extract the probability
        probability = obj.result.general_probability;
        // get the algorithims result
        alg_result = obj.result.algorithms;
        console.log('-----------The algorithm result-----------');
        console.log(alg_result);
        // get the total time it takes to fetch the result in seconds
        var final_time = new Date().getTime();
        time_taken = (final_time - start_time) / 1000;
        total_logs.push("Time taken in seconds: "+time_taken); 
        // set data to local storage
        back_set_data(currenturl, callUUID, deviceUUID, probability, time_taken, alg_result, escrow_info, agi_info, errorMsg, call_id, trace_id);
      })
      .catch(error => {
          if(typeof(error) === "object"){
              if(result === "") { errorMsg = "Empty result returned"; }
              else { errorMsg = error.message; }
          } else {
              errorMsg = error;
          }
          console.log("error: ", error);
          total_logs.push("-----Error Occured while fetching/parsing the result from Api----");
          total_logs.push(errorMsg + " : " + error.stack);
          // get the total time it takes to fetch the result in seconds
          var final_time = new Date().getTime();
          time_taken = (final_time - start_time) / 1000;
          total_logs.push("Time taken in seconds: "+time_taken); 

          // set data to local storage
          back_set_data(currenturl, callUUID, deviceUUID, probability, time_taken, alg_result, escrow_info, agi_info, errorMsg, call_id, trace_id);
      });
}


/*
* set data to the local database
*/
function back_set_data(currenturl, callUUID, deviceUUID, probability, time_taken, algorithims, escrow_info, agi_info, error, call_id, trace_id) {
    // then update the local with latest value
    const status="done";
    const rating = {"all":0}; // set rating
    const feedback = ""; // set feedback
    const timestamp = new Date().getTime();
    // round probability
    probability = probability.toFixed(2);
    const rank = probability; // set rank with returned probability at the first time
    console.log("Rounded Probability: ", probability, " rank, ", rank);
    const resultobject = {url:currenturl, 
                          callUUID:callUUID,
                          deviceUUID:deviceUUID,
                          status:status, 
                          time_taken:time_taken, 
                          timestamp:timestamp,
                          probability:probability, 
                          algorithims:algorithims, 
                          escrow_info:escrow_info,
                          agi_info:agi_info,
                          rating: rating,
                          feedback: feedback,
                          rank: rank,
                          error: error,
                          call_id:call_id,
                          trace_id:trace_id
                         };
    
    // push to local data
    push_data(currenturl, timestamp, call_id, trace_id, resultobject);
  }
  
/*
* push data to the local database
* @parms: currenturl, result
*/
function push_data(currenturl, timestamp, call_id, trace_id, resultobject) {
    chrome.storage.local.get(['NData'], function(result) {
        const parseData = result['NData'];
        // create list to hold list of data
        var listdata = [];
        if (parseData){
            // loop through data and add them to the list
            for (const elt in parseData) {
              // remove the current url with pending status and update it with done status
              var storedurl = parseData[elt].url;
              if (currenturl == storedurl){
                  // if so skip dont add to the list, it will be appended with the updated one at the end
              }
              else{
                  listdata.push(parseData[elt]); 
              }
            }
        }
        else {
          // initialize empty data
        }
        // push the current one the list
        listdata.push(resultobject);
        chrome.storage.local.set({'NData':listdata}, function() {
            if(chrome.runtime.lastError){
              console.log("Error from background js while pushing to local database: ", chrome.runtime.lastError);
              total_logs.push("Error while pushing to local database:"+chrome.runtime.lastError)
              return;
            }
            console.log("data is stored");
            total_logs.push("Data is stored into local database");
            console.log("data: ", listdata);
            // then now add the total logs
            push_log(currenturl, timestamp, call_id, trace_id);
            // then revert icon to the original
            chrome.browserAction.setIcon({path: "./assets/img/lyingface.png"});
            setTimeout(
              function(){
                back_check_status();
              }, 1000
           );
        });
    });
    
}

/*
* initialize the log local database
* @params: log
*/
function push_log(currenturl, timestamp, call_id, trace_id) {
  chrome.storage.local.get(['Logs'], function(result) {
      const parseLogs = result['Logs'];
      // create list to hold list of logs
      var data = [];
      // if found push to it if not initialize
      if (parseLogs){
        for (const elt in parseLogs) {
          // append the logs to logs variable
          data.push(parseLogs[elt]);          
        }
      }
    
      result = {url:currenturl, timestamp:timestamp, call_id:call_id, trace_id, logs:total_logs};
      data.push(result);
      // now delete the data in total_logs
      total_logs = [];
      chrome.storage.local.set({'Logs':data}, function() {
          if(chrome.runtime.lastError){
            console.log("Error from background js while pushing logs to local database: ", chrome.runtime.lastError);
            return;
          }
          console.log("log is stored");
          console.log("log data: ", data);
      });
  });  
}

