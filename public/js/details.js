// get the window screen width
var screenWidth = window.screen.width;

details();

function details(){
    // get current extension url
    var ext_url = location.href;
    var ext_url = new URL(ext_url);
    // get the parameters from current url
    var url = ext_url.searchParams.get("url");
    // update the details page dynamically based on the parameters of the url
    // alg is used to identify which algorithim is it
    // id is used to identify which call(url) is it
    chrome.storage.local.get('NData', function(result) {
        const parseData = result['NData'];
        var idx = 0;
        var call_id = ""; // variable to hold the call_id
        var e_info =[];
        var a_info =[];
        for (x in parseData) {
            var db_url = parseData[x].url;
            if (url == db_url) {
                console.log("parse data", parseData[x]);
                call_id = parseData[x].call_id;
                e_info = parseData[x].escrow_info;
                a_info = parseData[x].agi_info;
                // check escrow result
                if(e_info.length == 0){
                    // fetch from api
                    console.log("get the escrow from api");
                    get_escrow(call_id);
                } else {
                    // escrow is already in the local database
                }

                if(a_info.length < 3){
                    console.log("get the agi info from api");
                    get_agi(call_id);
                } else {
                    // agi info is already in the local database
                }
                 
                var result = "";
                // check the screen width and call the respective functions
                if (screenWidth > 800){
                    // add the header
                    result += createHeaderForGreaterThan800(parseData[x]);
                    result += createDetailsForGreaterThan800(parseData[x], idx);
                } else {
                    // add the header
                    result += createHeaderForLessThan800(parseData[x]);
                    result += createDetailsForLessThan800(parseData[x], idx);
                }
                // add the tbody to the created table in the details.html with id stance-json
                document.getElementById("details").innerHTML = result;
            }
            idx += 1;
        }
    });
}

/*
* get escrow data
* @params: call_id
*/
function get_escrow(call_id){
    // first check if escrow is not found in local db
    console.log("--------making request----Getting Escrow Data-----");
    const api_endpoint = "http://demo.nunet.io:7006/";
    const method = "get_escrow";
    const url_endpoint = api_endpoint+method+"?call_id="+call_id;
    console.log("request: ", url_endpoint);
    
    var escrow_info = []; // set escrow info
    
    // fetch the rest api
    fetch(url_endpoint)
    .then(response => response.text())
    .then(result => {
        // get the result value from the key call
        console.log("Escrow result: ", result);
        // check if the returned result is empty or not
        if (result === "") {
            console.log("The escrow info returned is empty");          
        } else {
          // remove the inappropriate double qoute which makes the dictionary value to be string, 
          // like "return":"{...}"     ,    "telemetry":"{...}"
          result = result.replaceAll(`"`, ``);
          // replace the single qoute with double qoute since single qoute has some issues while doing JSON parsing
          result = result.replaceAll(`'`, `"`);
          // get the escrow info with parsing the result
          escrow_info = JSON.parse(result);
          // update each value to the parsed one
          for(i=0; i<escrow_info.length; i++){
              escrow_info[i] = JSON.parse(escrow_info[i]);
          }
          console.log("escrow: ", escrow_info);
        }
        // set escrow info to the local db
        set_escrow(call_id, escrow_info);
    })
    .catch(error => {
        console.log("error occured while fetching escrow---");
        if(typeof(error) === "object"){
            console.log(error.message);
        } else if(typeof(error) === "string"){
            console.log(error);
        } else {
          //
        }
    });
}

/* 
* set escrow info to local database
* @params: call_Id
*/
function set_escrow(call_id, info){
    // check if the info is empty or not, if its empty again call get_escrow until result returned
    if(info.length === 0){
        setTimeout(
            function(){
                console.log("checking get escrow again")
                get_escrow(call_id);
            }, 1000
         );
    }
    else {
        chrome.storage.local.get('NData', function(result){
            var parseData = result['NData'];
            for (var x in parseData) {
              var db_call_id = parseData[x].call_id;
              if (call_id === db_call_id){
                  parseData[x].escrow_info = info;
              }
            }
            // set the updated data to local
            chrome.storage.local.set({'NData':parseData}, function() {
              if(chrome.runtime.lastError){
                console.log("Error while storing escrow info: ", chrome.runtime.lastError);
                return;
              }
                console.log("escrow info is stored");
                console.log("updated data after escrow added: ", parseData);
                // reload the page to display the cost and escrow and token dropdown of each algorithim
                location.replace(location.href.split('#')[0]);
            });
          });
    }
}

/*
* get agi data
* @params: call_id
*/
function get_agi(call_id){
    // first check if escrow is not found in local db
    console.log("--------making request----Getting AGI Data-----");
    const api_endpoint = "http://demo.nunet.io:7006/";
    const method = "get_agi_txn";
    const url_endpoint = api_endpoint+method+"?call_id="+call_id;
    console.log("request: ", url_endpoint);
    
    var agi_info = []; // set agi info
    
    // fetch the rest api
    fetch(url_endpoint)
    .then(response => response.text())
    .then(result => {
        // get the result value from the key call
        // check if the returned result is empty or not
        if (result === "") {
            console.log("The agi info returned is empty");          
        } else {

          // remove the inappropriate double qoute which makes the dictionary value to be string, 
          // like "return":"{...}"     ,    "telemetry":"{...}"
          result = result.replaceAll(`"`, ``);
          // replace the single qoute with double qoute since single qoute has some issues while doing JSON parsing
          result = result.replaceAll(`'`, `"`);
          // get the agi info with parsing the result
          agi_info = JSON.parse(result);
          console.log("Agi info: ", agi_info);
        }
        // set agi info to the local db
        set_agi(call_id, agi_info);
    })
    .catch(error => {
        console.log("error occured while fetching agi---");
        if(typeof(error) === "object"){
            console.log(error.message);
        } else if(typeof(error) === "string"){
            console.log(error);
        } else {
          //
        }
    });
}

/* 
* set agi info to local database
* @params: call_Id
*/
function set_agi(call_id, info){
    // check if the info is empty or not, if its empty again call get_escrow until result returned
    console.log("agi agi agie");
    if(info.length < 3){
        setTimeout(
            function(){
                console.log("checking get agi info again")
                get_agi(call_id);
            }, 1000
         );
    }
    else {
        chrome.storage.local.get('NData', function(result){
            var parseData = result['NData'];
            for (var x in parseData) {
              var db_call_id = parseData[x].call_id;
              if (call_id === db_call_id){
                  parseData[x].agi_info = info;
              }
            }
            // set the updated data to local
            chrome.storage.local.set({'NData':parseData}, function() {
              if(chrome.runtime.lastError){
                console.log("Error while storing agi info: ", chrome.runtime.lastError);
                return;
              }
                console.log("agi info is stored");
                console.log("updated data after agi added: ", parseData);
                // reload the page to display the cost and escrow and token dropdown of each algorithim
                location.replace(location.href.split('#')[0]);
            });
        });
    }
}

// function to create the header for screen with greater than 800 width
function createHeaderForGreaterThan800(data){
    var url = data.url;
    var call_id = data.call_id;
    var date = getDate(data.timestamp)
    var time_taken = data.time_taken;
    var user_rating = data.rank;
    var fake_probability = data.probability;
    var trace_id = data.trace_id;
    var header = "";
    header += '<div class="container">';
      
      header += '<div class="row">';
        header += '<div class="col-2">';
        header += '</div>';
        header += '<div class="col-10">';
        header += '<h6><span style="font-size:20px;">Article:</span> <span><a id="url" style="color:#48A79C" target="_blank" href="'+url+'">'+url.substring(0, 50)+'</a></span></h6>';
        header += '</div>';  
      header += '</div>';  

      header += '<div class="row">';
        header += '<div class="col-2">';
        header += '<a href="./history.html">';
        header += '<button type="button" class="btn" style="background-color:#48A79C;color:white;margin-top:10px">Back</button>';
        header += '</a>';
        header += '</div>';
    
        header += '<div class="col-4">';      
        header += '<h6><span style="font-size:17px;">Date:</span> <span id="date">'+date+'</span></h6>'
        header += '<h6><span style="font-size:17px;">Time Taken:</span> <span id="time_taken">'+time_taken+'s</span></h6>';
        header += '<h6><span style="font-size:17px;"></span> <span id="raw_result"><a href="./rawresult.html?call_id='+call_id+'" target="_blank" style="color:#48A79C">Get Raw Result</a></span></h6>';
        header += '</div>';
        header += '<div class="col-4" >';
        header += '<h6><span style="font-size:17px;">Fake Probability:</span> <span id="fake_probability">'+fake_probability+'%</span></h6>';
        header += '<h6><span style="font-size:17px;">Is It Useful?:</span> <span id="user_rating">'+user_rating+'%</span></h6>';
        header += '<h6><span style="font-size:17px;">Trace Id:</span> <span id="trace_id"><a href="http://testserver.nunet.io:16686/trace/'+trace_id+'" style="color:#48A79C" target="_blank">'+trace_id+'</a></span></h6>';
        header += '</div>';
      header += '</div>';
    
    header += '</div>';  
    header += '<hr />';

    return header;

}

// function to create the header for screen with greater than 800 width
function createHeaderForLessThan800(data){
    var url = data.url;
    var date = getDate(data.timestamp)
    var time_taken = data.time_taken;
    
    var header = "";
    header += '<div style="color: black;">';
    header += '<h6><span>Article:</span><span><a id="url" target="_blank" style="color:#48A79C" href="'+url+'">'+url.substring(0, 50)+'</a></span></h6>';
    header += '<p style="font-size:15px;">Date: </span><span id="date">'+date+'</span></p>'
    header += '<p style="font-size:15px;">Time Taken:</span><span id="time_taken">'+time_taken+'s</span></p>';
    header += '<hr />';    
    header += '</div>';  

    return header;
}

// function to create collapsible and expandable components
function createDetailsForGreaterThan800(data, idx){
    // get url
    var url = data.url;
    // create the collapse(dropdown) for list of algorithims as found in local storage
    var collapse = ""; // initialize the collapse
    var algResult = data.algorithims;
    var algList = {}; // variable to hold the algorithims value
    
    for (var i=0; i < algResult.length; i++){
        var name = algResult[i].name;
        var readableName = algResult[i].human_readable
        // change the first letter of name to upper case
        name = name[0].toUpperCase() + name.substring(1);
        // add the algorithim name and value to the object
        algList[name] = readableName;
    } 

    var algNames = Object.keys(algList);
    var i = 0;
    for (let name of algNames){
        if (i % 2 == 0) {
            collapse += "<div class='container'><div class='row'>";
        }
        collapse += "<div class='col-5' style='background-color:#f4f7f8'>";
        collapse += "<div style='margin:10px;'><h5>"+ algList[name] +"</h5></div>";
        collapse += '<ul class="list-group">';
        
        collapse += "<li class='list-group-item' style='background-color:#f4f7f8'>";
        collapse += "<a name='openClose' value='"+name+"' data-toggle='collapse' data-target='#about-"+name+"' aria-expanded='false' href='#' style='color:black;text-decoration: none;'>";
        collapse += '<div class="container"><div class="row">';
        collapse += '<div class="col-10">About</div>';
        collapse += "<div class='col-2'><img src='../assets/img/expand.png' height='20px' /></div>";
        collapse += '</div></div>';
        collapse += "</a>";
        collapse += "<div>"+createAboutCollapse(data, name) +"</div>"
        collapse += "</li>";

        collapse += "<li class='list-group-item' style='background-color:#f4f7f8'>";
        collapse += "<a name='openClose' value='"+name+"' data-toggle='collapse' data-target='#jsonresult-"+name+"' aria-expanded='false' href='#' style='color:black;text-decoration: none;'>";
        collapse += '<div class="container"><div class="row">';
        collapse += '<div class="col-10">JSON Result</div>';
        collapse += "<div class='col-2'><img src='../assets/img/expand.png' height='20px' /></div>";
        collapse += '</div></div>';
        collapse += "</a>";
        collapse += "<div>"+createJsonResultCollapse(data, name) +"</div>"
        collapse += "</li>";
        
        collapse += "<li class='list-group-item' style='background-color:#f4f7f8'>";
        collapse += "<a name='openClose' value='"+name+"' data-toggle='collapse' data-target='#telemetry-"+name+"' aria-expanded='false' href='#' style='color:black;text-decoration: none;'>";
        collapse += '<div class="container"><div class="row">';
        collapse += '<div class="col-10">Telemetry</div>';
        collapse += "<div class='col-2'><img src='../assets/img/expand.png' height='20px' /></div>";
        collapse += '</div></div>';
        collapse += "</a>";
        collapse += "<div>"+createTelemetryCollapse(data, name) +"</div>"
        collapse += "</li>";
        
        collapse += "<li class='list-group-item' style='background-color:#f4f7f8'>";
        collapse += "<a name='openClose' value='"+name+"' data-toggle='collapse' data-target='#token-"+name+"' aria-expanded='false' href='#' style='color:black;text-decoration: none;'>";
        collapse += '<div class="container"><div class="row">';
        collapse += '<div class="col-10">Tokens</div>';
        collapse += "<div class='col-2'><img src='../assets/img/expand.png' height='20px' /></div>";
        collapse += '</div></div>';
        collapse += "</a>";
        collapse += "<div>"+createTokenCollapse(data, name) +"</div>"
        collapse += "</li>";     
        
        collapse += "</ul>";
        collapse += '<div class="container" style="margin-top:15px;margin-bottom:10px"><div class="row">';
        if(name !== "Testing-news-score" && name !== "News-score") {
            console.log("they are not the same");
            collapse += "<div class='col-4'>Is it useful?</div>"
            collapse += "<div class='col-8'>" + createSlider(name, url, idx) + "</div>"; 
        }
        collapse += '</div></div>'
        collapse += "</div>";

        if (i % 2 == 0){
            collapse += "<div class='col-1' style='background-color:white;'></div>"
        } else {
            collapse += "</div></div>";
            collapse += "<div style='background-color:#a8e2db;margin-top:20px;'></div>"
        }
        
        i += 1;
    }

    // add the feedback text to the collapse
    if (i%2 == 0) {
        collapse += '<div style="background-color:#f4f7f8;padding:10px;margin-left:30%;margin-right:30%">';
    } else {
        collapse += "<div class='col-5' style='background-color:#f4f7f8;padding:10px;'>";
    }
    
    collapse += '<div style="margin-left:10px;"><h5>Feedback</h5></div>';
    collapse += '<div><strong id="feedbackTitle"></strong><span id="feedback">'+getFeedback(url)+'</span></div>';
    collapse += '<div id="write-feedback">'
    collapse += '<div class="form-group shadow-textarea w-100" align="center">';
    collapse += '<textarea class="form-control z-depth-1" id="feedbackTxt" rows="4" placeholder="Write feedback here ..."></textarea>';
    collapse += '<p></p><a><button type="button" class="btn" style="background-color:#48A79C;color:white" name="feedbackBtn" value="" id="feedbackBtn">Submit Feedback</button></a>';
    collapse += '</div></div>';

    collapse += '</div>';

    return collapse;
}

function createDetailsForLessThan800(data, idx){
     // get url
    var url = data.url;
    // create the collapse(dropdown) for list of algorithims as found in local storage
    var collapse = ""; // initialize the collapse
    var algResult = data.algorithims;
    var algList = {}; // variable to hold the algorithims value
    
    for (var i=0; i < algResult.length; i++){
        var name = algResult[i].name;
        var readableName = algResult[i].human_readable
        // change the first letter of name to upper case
        name = name[0].toUpperCase() + name.substring(1);
        // add the algorithim name and value to the object
        algList[name] = readableName;
    } 

    var algNames = Object.keys(algList);

    for (let name of algNames){
        collapse += "<div style='background-color:#f4f7f8;padding:10px;'>";
            collapse += "<div><h6>"+algList[name]+"</h6></div>";
            collapse += '<ul class="list-group">';
            collapse += "<li class='list-group-item' style='background-color:#f4f7f8'>";
            collapse += "<a name='openClose' value='"+name+"' data-toggle='collapse' data-target='#jsonresult-"+name+"' aria-expanded='false' href='#' style='color:black;text-decoration: none;'>";
            collapse += '<div class="container"><div class="row">';
            collapse += '<div class="col-10">JSON Result</div>';
            collapse += "<div class='col-2'><img src='../assets/img/expand.png' height='20px' /></div>";
            collapse += '</div></div>';
            collapse += "</a>";
            collapse += "<div>"+createJsonResultCollapse(data, name) +"</div>"
            collapse += "</li>";
            collapse += "<li class='list-group-item' style='background-color:#f4f7f8'>";
            collapse += "<a name='openClose' value='"+name+"' data-toggle='collapse' data-target='#telemetry-"+name+"' aria-expanded='false' href='#' style='color:black;text-decoration: none;'>";
            collapse += '<div class="container"><div class="row">';
            collapse += '<div class="col-10">Telemetry</div>';
            collapse += "<div class='col-2'><img src='../assets/img/expand.png' height='20px' /></div>";
            collapse += '</div></div>';
            collapse += "</a>";
            collapse += "<div>"+createTelemetryCollapse(data, name) +"</div>"
            collapse += "</li>";
            collapse += "<li class='list-group-item' style='background-color:#f4f7f8'>";
            collapse += "<a name='openClose' value='"+name+"' data-toggle='collapse' data-target='#token-"+name+"' aria-expanded='false' href='#' style='color:black;text-decoration: none;'>";
            collapse += '<div class="container"><div class="row">';
            collapse += '<div class="col-10">Tokens</div>';
            collapse += "<div class='col-2'><img src='../assets/img/expand.png' height='20px' /></div>";
            collapse += '</div></div>';
            collapse += "</a>";
            collapse += "<div>"+createTokenCollapse(data, name) +"</div>"
            collapse += "</li>";
            collapse += "</ul>";
            collapse += '<div class="container" style="margin-top:15px;"><div class="row">';
            collapse += '</div></div>'
        collapse += "</div>";
        collapse += "<div style='background-color:#0D1B41;margin-top:20px;'></div>";
    }

    // add the feedback text to the collapse
    collapse += '<div style="background-color:#f4f7f8;padding:5px;>';
        collapse += '<div style="margin-left:10px;"><h6>Feedback</h6></div>';
        collapse += '<div style="background-color:#f4f7f8;padding-left:5px;"><strong id="feedbackTitle"></strong><span id="feedback">'+getFeedback(url)+'</span></div>';
        collapse += '<div id="write-feedback">'
        collapse += '<div class="form-group shadow-textarea w-100" style="background-color:#f4f7f8;padding:10px">';
        collapse += '<textarea class="form-control z-depth-1" id="feedbackTxt" rows="4" placeholder="Write feedback here ..."></textarea>';
        collapse += '<div align="center" style="background-color:#f4f7f8;padding:10px;">';
        collapse += '<a><button type="button" class="btn" style="background-color:#48ada2;color:white" name="feedbackBtn" value="" id="feedbackBtn">Submit Feedback</button>';
        collapse += '</a>';
        collapse += '<a style="margin-left:10px;" href="./history.html">';
        collapse += '<button type="button" class="btn" style="background-color:#48A79C;color:white;">Back</button>';
        collapse += '</a>';
        collapse += '</div>';
        collapse += '</div></div>';
    collapse += '</div>';
        
    return collapse;
}

// To get date string from timestamp
function getDate(timestamp){
    
    var date = new Date(+timestamp); // use + before a vraible name in Date object
    // get time string
    var time_string = date.toLocaleTimeString();
    // initialize date string
    var date_string;

    var day_ms = 24 * 60 * 60 * 1000; //nb millis in a day
    var today_ms = new Date().setHours(0,0,0,0);
    // get the difference between the timestamp from database and today timestamp
    var diff = timestamp - today_ms;
    
    // check the difference, if its less than day time stamp, then display today, if not display date string
    if (diff >= 0 && diff <= day_ms) {
        var date_string = "Today";
    } else {
        var date_string = date.toLocaleDateString();
    }
    
    return date_string + ", " + time_string;
}


// To add the trigger event on click
// specifically for collapse, rating and feedback on the history page.
window.onclick = e => {
    var name = e.target.name;
    console.log("name, ", name);
    // get current extension url
    var ext_url = location.href;
    var ext_url = new URL(ext_url);
    // get the parameters from current url
    var alg = ext_url.searchParams.get('alg');
    var url = ext_url.searchParams.get("url");
     
    // check if the feedback submit button is clicked
    if (name == "feedbackBtn") {
        console.log("name: ", name);
        //var alg = e.target.value;
        // get the value of the button which is the id of the call
        setFeedback(url);
    }

    // check if its the copy docker image icon
    if (name == "copyDockerImageBtn") {
        console.log("name: ", name);
        var copyId = e.target.id;
        var algId = copyId.split("#")[1];
        copyText("dockerImage-"+algId, algId);
    }

    if (name == "slider") {
        console.log("name: ", name);
        // get the value from the range
        var value = e.target.value;
        // get the url call id from the target
        var id = e.target.id;
        var splt = id.split("::");
        var alg = splt[0];
        var callId = splt[1];
        console.log("algorithim: ", alg);
        console.log("call id: ", callId);
        console.log("value value: ", value);

        setRating(alg, url, value);   
    }
}

function createAboutCollapse(data, algName){
    var algResult = data.algorithims;
    var collapse = "";
    collapse += "<div class='collapse' id='about-"+algName+"'>";
    
    for (var i=0; i < algResult.length; i++){
        var name = algResult[i].name;
        // capitalize the name as already the capitalized name is coming from UI
        name = name[0].toUpperCase() + name.substring(1);
        if (name === algName) {
            // display the information about the algoritim            
            //collapse += "<div><small><strong>Name</strong> : "+algResult[i].name+"</small></div>";
            collapse += "<div><small><strong>Type</strong> : "+algResult[i].type+"</small></div>"
            collapse += "<div><small><strong>Snet Service Id</strong> : "+algResult[i].snet_service_id+"</small></div>";
            collapse += "<div><small><strong>Version</strong> : "+algResult[i].version+"</small></div>"; 
            collapse += "<div><small><strong>Description</strong> : "+algResult[i].description+"</small></div>";
            collapse += "<div><small><strong>Source Repo</strong> : <a href='"+algResult[i].src_repo+"' target='_blank' style='color:#48A79C'>"+algResult[i].src_repo+"</a></small></div>";
            collapse += "<div><small><strong>Docker image</strong> : <span id='dockerImage-"+name+"'>"+algResult[i].image+"</span></small>";
            collapse += "&nbsp;<img name='copyDockerImageBtn' id='copy#"+name+"' title='Click to copy' src='../assets/img/clipboard.svg' />";
            collapse += "<small class='tooltiptext' id='docker-tooltip-"+name+"'></small></div>";
            //collapse += "<div><small><strong>Updated timestamp</strong> : "+getDate(algResult[i].update_timestamp)+"</small></div>";
        }
    }
    collapse += "</div>";

    return collapse;
}

/**
 * Function to write current text to clip board, esp for docker image
 * @param {*} element 
 */
function copyText(element,name) {
    var copyElement = document.getElementById(element);
    navigator.clipboard.writeText(copyElement.textContent)
    .then(function() {
        // if successful show copied tooltip message and hide after 2 seconds
        var tooltip = document.getElementById('docker-tooltip-'+name);
        tooltip.style.visibility = "visible";
        tooltip.textContent = "Copied!";
        setTimeout(
            function(){
              tooltip.style.visibility = "hidden";
            }, 2000
         );
    }, function() {
        // if failed to write to clip board
    })
  }

function createJsonResultCollapse(data, algName){ 
    var algResult = data.algorithims;
    var collapse = "";
    collapse += "<div class='collapse' id='jsonresult-"+algName+"'>";
    
    for (var i=0; i < algResult.length; i++){
        var name = algResult[i].name;
        // capitalize the name as already the capitalized name is coming from UI
        name = name[0].toUpperCase() + name.substring(1);

        if (name === algName) {
            // get the return dictionary of the algorithim
            var algReturn = algResult[i].return;

            // display the string of raw json result as its instead of detecting key and value
            var tostring = JSON.stringify(algReturn);
            var result = '';
            while (tostring.length > 0) {
              result += tostring.substring(0, 43) + "\n";
              tostring = tostring.substring(43);
            }
            collapse += "<div><small>"+result+"</small></div>";
            
            // for (var key in algReturn) {
            //     // get the value
            //     var value = algReturn[key];       
            //     // capitalize the first key
            //     var capitalizeKey = key[0].toUpperCase() + key.substring(1);           
            //     collapse += "<div><small>"+capitalizeKey+" : "+value+"</small></div>";
            // }
        }
    }
    collapse += "</div>";

    return collapse;
}

function createTelemetryCollapse(data, algName){
    var algResult = data.algorithims;
    var collapse = "";
    collapse += "<div class='collapse' id='telemetry-"+algName+"'>";
    
    for (var i=0; i < algResult.length; i++){
        var name = algResult[i].name;
        // capitalize the name as already the capitalized name is coming from UI
        name = name[0].toUpperCase() + name.substring(1);
        if (name === algName) {
            // get the telemetry value of the algorithim
            var algTelemetry = algResult[i].telemetry;
            for (var key in algTelemetry) {
                // get the value and change to two decimal place
                var value = algTelemetry[key].toFixed(2);  

                var info = ""; // variable to hold information of each telemetry

                // add the metric to the value
                if (key === "cpu used"){
                    value = value + " Mticks";
                    info = "Aggregated CPU usage of all machines in the network, measured in million CPU ticks. MTicks (million ticks) metric shows how much CPU time was used during the execution of the measured process.";
                } else if (key === "memory used") {
                    value = value + " MB";
                    info = "The metric of memory used by a computational process. It is measured in MBs (megabyte seconds) which is calculated by adding spot RAM usages sampled every second for the whole Time of execution.";
                } else if (key === "network used") {
                    value = value + " KB";
                    info = "The sum of data transferred and received via network interfaces during process execution, in KB (kilobytes);";
                } else if (key === "time_taken") {
                    value = value + " sec";
                    info = "The wall time of the process execution measured in seconds.";
                } else {
                    //
                }

                // capitalize the first key
                var capitalizeKey = key[0].toUpperCase() + key.substring(1);           
                // add tooltip with description, attach help icon, then key, then value
                collapse += '<div><small>'+
                    '<a href="#" data-toggle="tooltip" data-placement="top" title="'+info+'" style="color:black;text-decoration:none;">'+
                    '<img src="../assets/img/info-circle.svg" />&nbsp;'+
                    '<strong>'+capitalizeKey+'</strong> : '+value+
                    '</a>'+
                    '</small></div>';
            }
        }
    }

    collapse += "</div>";


    return collapse;
}

function createTokenCollapse(data, algName){
    var escrowResult = data.escrow_info;
    var agiResult = data.agi_info;
    var collapse = "";
    // check if escrow result is returned or not
    collapse += "<div class='collapse' id='token-"+algName+"'>";
    if (escrowResult.length !== 0) { 
        for (var i=0; i < escrowResult.length; i++){
            var service = escrowResult[i].escrow_address.service;
            // capitalize the name as already the capitalized name is coming from UI
            service = service[0].toUpperCase() + service.substring(1);
            // check if the algorithim name and service is the same to get the escrow info
            if (service === algName) {
                // get the telemetry value of the algorithim
                var cost = escrowResult[i].escrow_address.cost;
                var escrow_address = escrowResult[i].escrow_address.escrow_address;
                collapse += "<div><small><strong>Cost</strong> : "+cost+" NTX</small></div>";
                collapse += '<div><small><strong>Escrow Address</strong> : <a href="http://testserver.nunet.io:3001/contracts/'+escrow_address+'" style="color:#48A79C" target="_blank">'+escrow_address+'</a></small></div>';
            }
        }
    }
    else {
        collapse += "<div><small><strong>Cost</strong> : <span id='cost-"+algName+"'>fetching...</span></small></div>";
        collapse += '<div><small><strong>Escrow Address</strong> : <span target="_blank" id="escrow-'+algName+'">fetching...</span></small></div>';
    }

    // check if agi txn is resulted or not
    if(agiResult.length !== 0) {
        for (var i=0; i < agiResult.length; i++){
            var service = agiResult[i].service_name;
            // capitalize the name as already the capitalized name is coming from UI
            service = service[0].toUpperCase() + service.substring(1);
            // check if the algorithim name and service is the same to get the escrow info
            if (service === algName) {
                // get the telemetry value of the algorithim
                var agi_amount=agiResult[i].amount;
                var agi_txn_hash=agiResult[i].txn_hash;
                collapse += "<div><small><strong>AGIX Cost</strong> : "+agi_amount+" TXN</small></div>";
                collapse += '<div><small><strong>AGIX Address</strong> : <a href="https://ropsten.etherscan.io/tx/'+agi_txn_hash+'" style="color:#48A79C" target="_blank">'+agi_txn_hash+'</a></small></div>';
            }
        }
    } else {
        collapse += "<div><small><strong>AGIX Cost</strong> : <span id='agi-amount-"+algName+"'>fetching...</span></small></div>";
        collapse += '<div><small><strong>AGIX Address</strong> : <span id="agi-hash-'+algName+'">fetching...</span></small></div>';
    }
    collapse += "</div>";
    return collapse;
}


// slider rating function for each algorithim
function createSlider(alg, url, callId) {
    var slider = "";
    slider += '<div class="d-flex">';
    slider += '<div class="w-75" >';
    slider += '<input type="range" name="slider" class="myinput" id="'+alg+'::'+callId+'" min="0" max="100" step="2" value="'+getRating(alg, url, callId)+'">';
    slider += '</div>';
    slider += '<span class="color:white;" id="slider-display-'+alg+'"></span>';
    slider += '</div>';

    return slider;
}

// To set rating three things should be identified
// 1. which algorithim 2. the call id 3. value of the rating
function setRating(alg, url, value){
    // change the value of rating to percent

    console.log("rating value: " + value);

    chrome.storage.local.get('NData', function(result) {
        const parseData = result['NData'];
        console.log('-----before updation-----');
        for (x in parseData) {
            var db_url = parseData[x].url;
            // update if the call id is the same
            if (db_url == url) {
                // console log result before updating
                console.log("get rating get rating get rating-", parseData[x].rating);
                // now update
                parseData[x].rating[alg] = value;
                // set the display slider text
                document.getElementById("slider-display-"+alg).innerHTML = value+"%";
            }
        }

        // set the updated data
        chrome.storage.local.set({'NData':parseData}, function(){
            console.log("rating is updated");
        });

        // check by getting the data
        chrome.storage.local.get('NData', function(result){
            const pd = result['NData'];
            console.log('----after updation----');
            for(x in pd){
                //console.log("rating after updation:", pd[x].rating);
                if (pd[x].url == url) {
                    console.log(parseData[x].rating);
                }
            }
        });

    });
}


// to get rating from local storage based on algorithim and call id
function getRating(alg, url, callId){
    var value = 0;
    // check by getting the data
    chrome.storage.local.get('NData', function(result){
        const pd = result['NData'];
        for(x in pd){
            //console.log("rating after updation:", pd[x].rating);
            if (pd[x].url == url) {
                value = pd[x].rating[alg];
                // if rating is not yet set just make it 0
                if (value == undefined){
                    value = 0;
                }
                console.log("new rating value", value)
                // set the display slider text
                document.getElementById("slider-display-"+alg).innerHTML = value+"%";
                document.getElementById(alg+'::'+callId).value = value;
            }
        }
    });
}
// get feedback and save it to local storage
function setFeedback(url){
    // get feedback from textarea
    var feedback = document.getElementById("feedbackTxt").value;
    console.log("feedback: "+feedback);
    
    chrome.storage.local.get('NData', function(result) {
        const parseData = result['NData'];
        console.log('---before updation-----');
        for (x in parseData) {    
            // update if the call id is the same
            if (parseData[x].url == url) {
                // display before updating
                console.log(parseData[x]);
                // now update
                if (feedback.length === 0) {
                    document.getElementById("feedback").innerHTML = "";
                    document.getElementById("feedbackTxt").value = "";
                }
                else {
                    parseData[x].feedback = feedback;
                    document.getElementById("feedbackTitle").innerHTML = "Submitted response: ";
                    document.getElementById("feedback").innerHTML = feedback;
                    document.getElementById("feedbackTxt").value = ""; 
                    document.getElementById("feedbackTxt").placeholder = "Update feedback here ..."; 
                }       
            }
        }

        // set the updated data
        chrome.storage.local.set({'NData':parseData}, function(){
            console.log("feedback is updated");
        });

        // check by getting the data
        chrome.storage.local.get('NData', function(result){
            const pd = result['NData'];
            console.log('----after updation----');
            for (x in parseData) {        
                // update if the call id is the same
                if (parseData[x].url == url) {
                    // display before updating
                    console.log(parseData[x]);
                }
            }
        });

    });
}


function getFeedback(url){
    // check by getting the data
    chrome.storage.local.get('NData', function(result){
        const pd = result['NData'];
        for(x in pd){
            //console.log("rating after updation:", pd[x].rating);
            if (pd[x].url == url) {
                var value = pd[x].feedback;
                if (value.length === 0){
                    value = "";
                    document.getElementById('feedback').innerHTML = value;
                }
                else {
                    console.log("feedback: ", value);
                    document.getElementById("feedbackTitle").innerHTML = "Already submitted response: ";
                    document.getElementById('feedback').innerHTML = value;
                    document.getElementById("feedbackTxt").placeholder = "Update feedback here ..."; 
                }
                
            }
        }
    });
}