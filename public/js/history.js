// get the window screen width
var screenWidth = window.screen.width;
console.log("window screen: ", screenWidth);

// check the screen width and call the respective functions
if (screenWidth > 800){
    history_for_greater_than_800();
} else {
    history_for_less_than_800();
}

/* Function to be implemented if the screen width is greater than 800 */
function history_for_greater_than_800(){
    var table = "";
    // initialize table
    table += "<table class='table table-borderless history-table'>";
    // add table heading
    table += "<thead>";
    table += "<tr> \
                <td scope='col'></td> \
                <td scope='col'>Website Article</td> \
                <td scope='col'>Date</td> \
                <td scope='col'>AGIX Cost</td> \
                <td scope='col'>NTX Cost</td> \
                <td scope='col'>Fake Probability</td> \
                <td scope='col'>User Rating</td> \
                <td scope='col'></td> \
               </tr>";
    table += "</thead>";
    // table body
    table += "<tbody>";
    // extract data from local storage and add them as td wrt to their heading
    chrome.storage.local.get('NData', function(result) {
        const parseData = result['NData'];
        console.log('data, ' + parseData);
        var i = 1;
        for (x in parseData) {
            table += "<tr>";
                table += "<th scope='row'>" + i + "</th>"; 
                table += "<td>" + "<a href="+parseData[x].url+" target='_blank' style='color:#48A79C'>"+(parseData[x].url).substring(0, 20)+"...</a></td>";
                table += "<td>" + getDate(parseData[x].timestamp) + "</td>";
                table += "<td>" + getAGI(parseData[x]) + "</td>";
                table += "<td>" + getCosts(parseData[x]) + "</td>";
                table += "<td id='fake-probability-"+i+"'>" + getFakeProbability(i) + "</td>";
                table += "<td id='user-rating-"+i+"'>" + getUserRating(i) + "</td>";
                table += '<td><a href="./details.html?url='+parseData[x].url+'"><button type="button" class="btn" style="background-color:#48A79C;color:white;width:100%" id="detailsBtn-'+i+'" value="'+i+'">View Analysis</button></a></td>';
            table += "</tr>";
            i += 1; // increase the numbers
        }
        // add the closing table after looping finished
        table += "</tbody></table>";
        // update the inner html of history div with the data
        document.getElementById("history").innerHTML = table;
    }); 
}

/* function to be implemented if the screen width is less than 800 */
function history_for_less_than_800(){
    var urlList = "";
    urlList += "<div>";
    // extract data from local storage and add them as td wrt to their heading
    chrome.storage.local.get('NData', function(result) {
        const parseData = result['NData'];
        console.log('data, ' + parseData);
        urlList += '<ul class="list-group ">';
        var i = 0
        for (x in parseData) {
            urlList += '<li class="list-group-item history-mobile-list">';
            urlList += '<div class="container" name="openCollpase" value="'+i+'" data-toggle="collapse" data-target="#data-'+i+'" aria-expanded="false" aria-controls="data">';
                urlList += '<div class="row">';
                urlList += '<div class="col-10"><small>'+(parseData[x].url)+'</small></div>';
                urlList += '<div class="col-2"><img src="../assets/img/expand.png" height="20px"></div>';
                urlList += '</div>';
            urlList += '</div></li>';
            urlList += collapseForLessThan800(parseData[x], i);
            urlList += '<div></div>';
            i += 1;
        }
        // add the closing table after looping finished
        urlList += "</ul></div>";
        // update the inner html of history div with the data
        document.getElementById("history").innerHTML = urlList;
    }); 
}

function collapseForLessThan800(data, index){
    // create the collase(dropdown) for useful data on the history page
    var collapse = "";
    collapse += '<div class="collapse" id="data-'+index+'" style="background-color:#f4f7f8">';
      collapse += '<div style="margin-bottom:10px;margin-top:10px;">';   
        collapse += '<table class="table table-borderless">';
        collapse += '<tbody style="color:black">';
        
        collapse += '<tr>';
        collapse += '<td scope="col">Date</td>';
        collapse += '<td scope="col">'+getDate(data.timestamp)+'</td>';
        collapse += '</tr>';

        collapse += '<tr>';
        collapse += '<td scope="col">Costs</td>';
        collapse += '<td scope="col">'+getCosts(data)+' NTXd</td>';
        collapse += '</tr>';

        collapse += '<tr>';
        collapse += '<td scope="col">Rate</td>';
        collapse += '</tr>';

        collapse += '<tr>';
        collapse += '<td scope="col"></td>';
        collapse += '<td scope="col"><a href="./details.html?url='+data.url+'"><button type="button" class="btn" style="background-color:#48A79C;color:white;" id="detailsBtn-'+index+'" value="'+index+'">View Analysis</button></a></td>';
        collapse += '</tr>';

        collapse += '</tbody>';
        collapse += '</table>';

      collapse += '</div>';   
    collapse += '</div>';

    return collapse;
}

// To get date string from timestamp
function getDate(timestamp){
    var date = new Date(+timestamp); // use + before a vraible name in Date object
    // get time string
    var time_string = date.toLocaleTimeString();
    // initialize date string
    var date_string;
    var day_ms = 24 * 60 * 60 * 1000; //nb millis in a day
    var today_ms = new Date().setHours(0,0,0,0);
    // get the difference between the timestamp from database and today timestamp
    var diff = timestamp - today_ms;
    // check the difference, if its less than day time stamp, then display today, if not display date string
    if (diff >= 0 && diff <= day_ms) {
        var date_string = "Today";
    } else {
        var date_string = date.toLocaleDateString();
    }  
    return date_string + ", " + time_string;
}

// get costs
function getCosts(data){
    var escrowResult = data.escrow_info;
    console.log("escrow result", escrowResult);
    var cost = 0;
    if (escrowResult.length === 0) {
        cost = "NaN";    
    } else {
        for (var i=0; i < escrowResult.length; i++){
            cost += parseInt(escrowResult[i].escrow_address.cost);
        }
    }
    console.log("cost, ", cost);
    return cost;
}

// get agi amount
function getAGI(data) {
    var agiResult = data.agi_info;
    console.log("agi result: ", agiResult);
    var amount = 0;
    if (agiResult.length === 0) {
        amount="NaN";
    } else {
        for (var i=0; i<agiResult.length; i++){
            console.log("amount amount amount: ", agiResult[i].amount);
            amount += parseFloat(agiResult[i].amount);
        }
    }
    console.log("amount: ", amount);
    return amount;
}


// To add the trigger event on click
// specifically for slider 
window.onclick = e => {
    var name = e.target.name;
    console.log("name, ", name);
    
    if (name == "slider") {
        console.log("name: ", name);
        // get the value from the range
        var value = e.target.value;
        // get the url call id from the target
        var id = e.target.id;
        var splt = id.split("::");
        var callId = splt[1];
        setRating(callId, value);
    }
}



// To set rating two things should be identified
// 1.the call id 2. value of the rating
function setRating(callId, value){
    // change the value of rating to percent
    chrome.storage.local.get('NData', function(result) {
        const parseData = result['NData'];
        console.log('-----before updation-----');
        var i = 1;
        for (x in parseData) {
            // update if the call id is the same
            if (i == callId) {
                // console log result before updating
                console.log(parseData[x].rank);
                // now update
                parseData[x].rank = value;
                // set the display slider text
                document.getElementById("slider-display-"+callId).innerHTML = value+"%";
            }
            i++;
        }
        // set the updated data
        chrome.storage.local.set({'NData':parseData}, function(){
            console.log("rating is updated");
        });
        // check by getting the data
        chrome.storage.local.get('NData', function(result){
            const pd = result['NData'];
            console.log('----after updation----');
            var i = 1;
            for(x in pd){
                if (i == callId) {
                    console.log(parseData[x].rank);
                }
                i++;
            }
        });
    });
}

// to get user rating from local storage based on call id
function getUserRating(callId){
    value = 0;
    // check by getting the data
    chrome.storage.local.get('NData', function(result){
        const pd = result['NData'];
        var i = 1;
        for(x in pd){
            //console.log("rating after updation:", pd[x].rating);
            if (i == callId) {
                value = pd[x].rank;

                if (value == undefined){
                    value = 0;
                }
                console.log("user rating value", value)
                // set the display slider text
                document.getElementById("user-rating-"+callId).innerHTML = value+"%";
            }
            i++;
        }
    });
}

// to get the fake probability from local storage based on call id
function getFakeProbability(callId){
    value = 0;
    // check by getting the data
    chrome.storage.local.get('NData', function(result){
        const pd = result['NData'];
        var i = 1;
        for(x in pd){
            //console.log("rating after updation:", pd[x].rating);
            if (i == callId) {
                value = pd[x].probability;

                if (value == undefined){
                    value = 0;
                }
                console.log("probability value", value)
                // set the display slider text
                document.getElementById("fake-probability-"+callId).innerHTML = value+"%";
            }
            i++;
        }
    });
}
