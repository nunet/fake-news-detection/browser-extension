
logdisplay();

function logdisplay(){
    
    chrome.storage.local.get('Logs', function(result) {
        const parseData = result['Logs'];
        var result = JSON.stringify(parseData, undefined, 4);
        // add the stringify result to the full log
        document.getElementById("logdisplay").innerHTML = result;
    });
}
