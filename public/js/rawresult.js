
rawresult();

function rawresult(){
    // get current extension url
    var ext_url = location.href;
    var ext_url = new URL(ext_url);
    // get the parameters from current url
    var call_id = ext_url.searchParams.get("call_id");

    chrome.storage.local.get('NData', function(result) {
        const parseData = result['NData'];
        var idx = 0;
        for (x in parseData) {
            var db_call_id = parseData[x].call_id;
            if (call_id == db_call_id) {
                var result = JSON.stringify(parseData[x], undefined, 4);
                // add the stringify result to the rawresult div
                document.getElementById("rawresult").innerHTML = result;
            }
            idx += 1;
        }
    });
}
