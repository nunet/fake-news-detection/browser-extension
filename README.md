# Browser extension for Fake News Warning App

We provide this chrome browser extension for usage by fake news warning app that in the backend runs SingularityNET AI Services on the NuNet provisioned hardware. The browser extension takes the url of the currently browsed article and returns the probability that the article contains fake news.

### Installing the extension on your browser

1. Download the newest zip file from [releases](https://gitlab.com/nunet/fake-news-detection/browser-extension/-/releases) Files Section.
2. unzip the downloaded file

    ```unzip browser-extension-<version info>.zip```
3. In your chrome browser go to chrome://extensions/ 
4. Enable developer mode 
5. Click Load upacked button, on the left side
6. Choose the build folder from the files in step(2)
7. Then you will get the extension icon on the app-bar


### Usage

1. Click the extension after you opened the article in chrome.

![Alt Text](public/assets/img/nunet.gif)




### To build and test
1. Clone the repo

    ```https://gitlab.com/nunet/fake-news-detection/browser-extension```
2. Get into the folder

    ``` cd browser-extension/```
3. Install 
    
    ```npm install```
4. Build

    ```npm run build```
5. Continiue installing the extension on your browser as the Installation section shows.

