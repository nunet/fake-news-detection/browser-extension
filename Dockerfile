# The node version from wich the app built
FROM node:16.1.0-alpine

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# add app
COPY . .

# install app dependencies
RUN npm install
RUN rm -rf build
RUN npm run build

RUN pwd
RUN ls